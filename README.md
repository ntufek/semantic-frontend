### Setup

To clone the repository on your computer 
```bash
$ git clone git@gitlab.com:ntufek/semantic-frontend.git
$ cd semantic-frontend
```

To install dependencies,
```bash
$ npm install
```

To run the project itself,
```bash
$ npm start
```