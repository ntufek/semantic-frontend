import React, { Component } from 'react';
import Main from './components/Main';
import {Container, Row, Col, Card, Button, InputGroup, FormGroup, FormControl,Image, Form } from "react-bootstrap";
import { Router, Route, hashHistory } from 'react-router';
import {Link} from 'react-router-dom'


class App extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
				
				<div className="App">
					<Main />
				</div>
		);
	}
}

export default App;
