
import React from 'react';
import {Link} from "react-router-dom"
import {Container, Row, Col, Card, Button, InputGroup, FormGroup, FormControl,Image, Form } from "react-bootstrap";

export default class Constants{
static bannerFunc(param)
{ 
    let homeCol = "black";
    let mypageCol="black";
    let createeventCol = "black";
    let votepeopleCol = "black";
    let signoutCol = "black";
    let colors = {
        "home":"red",
        "mypage":"black",
        "vote":"black",
        "signout":"black",
        "crateevent":"black",
    }
    console.log("from BANNER param:", param)
    //colors[param]= "cyan"

    return(<Row>
        <Col  md="3"/>
        <Col md="8">
            <Link to={{
                pathname: '/home',
                state: { detail: param}
                }} >
            <button  type="button">HOME</button>
            </Link>
            <Link to={{
                pathname: '/mypage',
                state: { detail: param}
                }}>
            <button  type="button">MY PAGE</button>
            </Link>
            <Link to={{
                pathname: '/createevent',
                state: { detail: param}
                }}>
            <button type="button">CREATE EVENT</button>
            </Link>
            <Link to={{
                pathname: '/votepeople',
                state: { detail: param}
                }}>
            <button type="button">VOTE PEOPLE</button>
            </Link>
            <Link to={{
                pathname: '/login',
                state: { detail: param}
                }}>
            <button  type="button" onClick={this.handleClickSignout}>SIGN OUT</button>
            </Link>
        </Col>
        <Col  md="1"/>
    </Row>);
}
}