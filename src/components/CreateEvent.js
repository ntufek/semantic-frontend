import React from 'react'
import {Link} from 'react-router-dom'
import {Container, Row, Col, Card, Button, InputGroup, FormGroup, FormControl,Image, Form } from "react-bootstrap";
import Constants from './Constants';

/*const activityTypeList = {
    Film:["FeatureFilm","ShortFilm"],
    Meeting:["CulturalMeeting","CustomMeeting","EducationalMeeting"],
    PerformingArts:["Ballet","Dance","Music","Opera","Theatre"],
    Sport:["IndivudialSport","TeamSport"],
};
*/
const activityTypeList=['Dance', 'SportsFilm', 'MuseumTour', 'Party', 'Badminton', 'Theatre', 'BallroomDance', 'TechnoMusic', 'Lunch', 'Hockey', 'VerismoOpera', 'CountryMusic', 'Candidate', 'InterestedActivityType', 'Dinner', 'MusicFilm', 'ThrillerShortFilm', 'SportsFilm', 'WarFilm', 'Name', 'ThrillerFilm', 'PerformingArts', 'JazzDance', 'RomanceTheatre', 'GrandOpera', 'Hockey', 'Athletics', 'Event1', 'Activity', 'ContemporaryBallet', 'RockMusic', 'Credibility', 'RomanticBallet', 'NeoclassicalBallet', 'RomanceTheatre', 'PopMusic', 'MotorSport', 'Conference', 'TableTennis', 'DocumentaryFilm', 'RomanceFilm', 'Basketball', 'FunkMusic', 'DocumentaryFilm', 'Swimming', 'TragicomedyTheatre', 'SeriousOpera', 'ClassicalMusic', 'ComedyTheatre', 'Workshop', 'DramaTheatre', 'BiographyFilm', 'RomanticBallet', 'HiphopMusic', 'ModernDance', 'WarFilm', 'Golf', 'FolkMusic', 'ReggaeMusic', 'BiographyFilm', 'ComedyTheatre', 'Tennis', 'JazzDance', 'RomanceShortFilm', 'ActionFilm', 'CorniqueOpera', 'HiphopDance', 'CorniqueOpera', 'AdventureFilm', 'HeavyMetalMusic', 'AlternativeRockMusic', 'BallroomDance', 'ScienceFictionFilm', 'Workshop', 'Age', 'Attended', 'ComedyShortFilm', 'NoirFilm', 'BluesMusic', 'ElectronicDanceMusic', 'Person1', 'DramaFilm', 'ComicOpera', 'TragedyTheatre', 'BachataDance', 'Breakfast', 'EducationalMeeting', 'AdventureTheatre', 'FunkMusic', 'GrandOpera', 'NoirFilm', 'SemiseriaOpera', 'IndividualSport', 'ActivityType', 'ArtFilm', 'Party', 'Music', 'Event2', 'AdventureTheatre', 'FantasyFilm', 'SoulMusic', 'TangoDance', 'RockMusic', 'RomanticComedyFilm', 'PopMusic', 'Other', 'Event1', 'SeriousOpera', 'DiscoMusic', 'TableTennis', 'WesternFilm', 'CountryMusic', 'Boxing', 'AnimationFilm', 'ThrillerFilm', 'Attendee', 'LatinDance', 'Football', 'Event2', 'ShortFilm', 'ClassicalBallet', 'Lunch', 'PunkRockMusic', 'Boxing', 'Badminton', 'HistoryFilm', 'HeavyMetalMusic', 'Dinner', 'DramaShortFilm', 'HistoryFilm', 'RomanticComedyFilm', 'FantasyFilm', 'Athletics', 'RomanceFilm', 'ClassicalBallet', 'MysteryFilm', 'ComedyFilm', 'CrimeFilm', 'SoulMusic', 'VerismoOpera', 'Basketball', 'ScienceFictionShortFilm', 'Person', 'HorrorFilm', 'ModernDance', 'ThrillerShortFilm', 'ContemporaryBallet', 'ComedyFilm', 'JazzMusic', 'RussianBallet', 'AnimationFilm', 'Meeting', 'Skiing', 'Seminar', 'Ballet', 'SemiseriaOpera', 'Volleyball', 'Gender', 'Exhibition', 'Opera', 'FeatureFilm', 'AnimationShortFilm', 'DramaTheatre', 'ComedyShortFilm', 'ScienceFictionShortFilm', 'Exhibition', 'ActionShortFilm', 'LatinDance', 'TragicomedyTheatre', 'NeoclassicalBallet', 'MysteryFilm', 'InstrumentalMusic', 'WesternFilm', 'IceHockey', 'AnimationShortFilm', 'HorrorFilm', 'TechnoMusic', 'Swimming', 'Person1', 'Golf', 'Location', 'PunkRockMusic', 'Football', 'Booked', 'FamilyFilm', 'CulturalMeeting', 'MusicFilm', 'Event', 'StartDateTime', 'HiphopMusic', 'HouseMusic', 'ClassicalMusic', 'ActionShortFilm', 'HorrorShortFilm', 'TeamSport', 'Volleyball', 'InterestedAgeMax', 'MuseumTour', 'FolkMusic', 'ReggaeMusic', 'FamilyShortFilm', 'Conference', 'MusicalFilm', 'ActionFilm', 'FamilyFilm', 'DiscoMusic', 'DocumentaryShortFilm', 'MusicalFilm', 'ComicOpera', 'BachataDance', 'AdventureFilm', 'ScienceFictionFilm', 'TragedyTheatre', 'IceHockey', 'Other', 'Skiing', 'Sport', 'JazzMusic', 'MotorSport', 'Performer', 'Film', 'RomanceShortFilm', 'Title', 'Seminar', 'ElectronicDanceMusic', 'InterestedGender', 'TangoDance', 'Tennis', 'FamilyShortFilm', 'BluesMusic', 'CustomMeeting', 'DocumentaryShortFilm', 'DramaFilm', 'HiphopDance', 'RussianBallet', 'InterestedAgeMin', 'HorrorShortFilm', 'ArtFilm', 'HouseMusic', 'CrimeFilm', 'Breakfast', 'DramaShortFilm', 'InstrumentalMusic', 'AlternativeRockMusic']


class CreateEvent extends React.Component {
    constructor(props)
    {
        super(props);
        this.state={
            title:"",
            location:"",
            performer:"",
            activityType:"",
            genreType:"",
            startDate:"",
            selectedGenreList : activityTypeList.Film,
        }
    }

    /*handleChangeGenre = (event) =>
    {
        console.log("event.target.value genre",event.target.value)
        this.setState({genreType: event.target.value});
    }*/

    handlesubmit = async(e) =>
    {
        console.log(this.state.title)
         e.preventDefault();

        this.props.history.push('/home');
        var tmpEvent = {
            "Title":this.state.title,
            "Location": this.state.location,
            "Performer": this.state.performer,
            "StartDateTime": this.state.startDate + "T12:50:46Z",
            "ActivityType": this.state.genreType
        }
        /*tmpEvent = {
            "Title": "Osman jr Concert",
            "Location": "Zorlu PSM",
            "Performer": "Shakira",
            "StartDateTime": "2019-11-26T12:50:46Z",
            "ActivityType": "PopMusic"
        }*/
        console.log("tmpEvent:", tmpEvent)
        let res = await fetch('http://127.0.0.1:8000/api/events/', {
            method: 'POST',
            headers: {
              //'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(tmpEvent)
        });
        console.log("res", res)
        
    }
    handleChange=(e)=>
    {
        console.log("this.state.title",this.state.title,this.state.location)
        console.log("e.name, e.value",e.target.name,e.target.value)
        this.setState({[e.target.name]: e.target.value});
    }


    handleChangeEvent = (event) =>
    {
        console.log("event.target.value event",event.target.value,this.state.selectedGenreList)

        this.setState({selectedGenreList: activityTypeList});

        /*if (event.target.value == "Film")
        {
            this.setState({activityType: event.target.value, selectedGenreList: activityTypeList.Film});
        }
        else if (event.target.value == "Meeting")
        {
            this.setState({activityType: event.target.value, selectedGenreList: activityTypeList.Meeting});
        }
        else if (event.target.value == "PerformingArts")
        {
            this.setState({activityType: event.target.value, selectedGenreList: activityTypeList.PerformingArts});
        }
        else if (event.target.value == "Sport")
        {
            this.setState({activityType: event.target.value, selectedGenreList: activityTypeList.Sport});
        }*/

    }

    render()
    {
        
        return (
            <div>
                {Constants.bannerFunc(this.props.location.state.detail)}
                <Container>
                <Row className="show-grid">
                    <Col md={2}></Col>
                    <Col md={8}>
                    <Card>
                        <Card.Body>
                            <Card.Title>Create New Event! </Card.Title>
               
			        <form onSubmit={this.handlesubmit}>
                    <label>
                        Title:
                        <input type="text" name="title" value={this.state.title} onChange={this.handleChange} />
                    </label>
                    <label>
                        Location:
                        <input type="text" name="location" value={this.state.location} onChange={this.handleChange} />
                    </label>
                    <label>
                        Performer:
                        <input type="text" name="performer" value={this.state.performer} onChange={this.handleChange} />
                    </label>
                    <label>
                        Start Date:
                        <input type="date" name="startDate" value={this.state.startDate} onChange={this.handleChange} />
                    </label>
                    
                    <label>
                        Activity Type:
                        <select name="genreType" value={this.state.genreType} onChange={this.handleChange}>
                        {activityTypeList.map(optn => (
                            <option>{optn}</option>
                        ))}
                        </select>
                    </label>
				
				        <Button type="submit" bsStyle="primary">Create</Button>
			        </form>
		        
                </Card.Body>
                    </Card>
                    
                    </Col>
                    
                    <Col md={2}></Col>
                </Row>
                </Container>
            
            </div>
        );

    }

}

export default CreateEvent;
