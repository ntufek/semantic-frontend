import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Login from './login/Login';
import RegistrationForm from './RegistrationForm';
import Home from './Home';
import MyPage from './MyPage';
import User from './user';
import CreateEvent from './CreateEvent';
import VotePeople from './VotePeople';

const Main = () => {
  return (
    
    <Switch>
      <Route exact path='/' component={Login}></Route>
      <Route exact path='/home' component={Home}></Route>
      <Route exact path='/login' component={Login}></Route>
      <Route exact path='/registrationform' component={RegistrationForm}></Route>
      <Route exact path='/mypage' component={MyPage}></Route>
      <Route exact path='/createevent' component={CreateEvent}></Route>
      <Route exact path='/votepeople' component={VotePeople}></Route>
    </Switch>
    
  );
}

export default Main;