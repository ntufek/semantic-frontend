import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {Container, Row, Col, Card, Button, InputGroup, FormGroup, FormControl,Image, Form } from "react-bootstrap";
import Home from './Home'
import User from './user'
import Constants from "./Constants"
import { isContainWhiteSpace } from '../shared/validator';
import Popup from "reactjs-popup";

/*var user_info={
    name:"Nilay",
    email:"ntufek@gmail.com",
    mobile:"5547062517",
    interests: ["theater","jazz"],
    prevEvents: Home.events,
    currentEvents: Home.global_events,

};*/
var glob_user={}
const GenderList = ["Male", "Female", "Else"];
class MyPage extends Component {

    constructor(props)
    {
        super(props);
        this.state={
            user: {},
            userselected:false,
            username: ""
        }
        
    }

    getUserInfox = async() =>
    {
        //console.log("state.detail heree",this.props.location.state.detail)
        let tmpusername= this.state.username
        if (this.state.username == "")
        {
            tmpusername = this.props.location.state.detail
        }

        try {
            //console.log("state.detail heree 1",this.props.location.state.detail)
            
            let response = await fetch(
              'http://localhost:8000/api/persons/'+tmpusername,
            );
            
            let responseJson = await response.json();
            //console.log("state.detail heree 2",this.props.location.state.detail)
            
            console.log("responseJson",responseJson)
            console.log("state.detail heree 3",tmpusername)
            
            
            console.log("state.detail heree 4",tmpusername)
            
            
            console.log("obareyy",this.state.userselected)
            this.setState({user:responseJson,userselected:true, username:tmpusername})
            return responseJson;
          } catch (error) {
            console.log("erorrr",error)
            
            console.error(error);
          }
          
    }
    SetMyInfo = () =>
    {

        
    }

    handleChange = (e) =>
    {
        console.log("this.state.title",this.state.title,this.state.location)
        console.log("e.name, e.value",e.target.name,e.target.value)
        this.setState({[e.target.name]: e.target.value});
    }
    handleSubmit = (e) =>
    {
        console.log("submitted")

        //this.forceUpdate()
        //this.props.history.push('/home');
        this.props.history.push({pathname:'/mypage',
        state: { detail: this.state.username }    
                    });
    }
    

  render() {
      if (!this.state.userselected)
      {
        let tmp = this.getUserInfox()
        
      }
    return (<div>


        {Constants.bannerFunc(this.state.username)}
        {this.state.userselected &&
        <Container>
                <Row className="show-grid">
                    <Col md={2}></Col>
                    <Col md={8}>
                    <Card>
                        {/*<Card.Img  style={{ width: '8rem' }} variant="top" src="https://fastly.4sqi.net/img/user/130x130/61495735_zEaJ0wk1_RHZofwIHkZ1TPpwmpSbpLAJ1tAgKbw94iT-62gVpyTzUglJYzEFptduugEQ3qb8j.jpg" />
                        */}
                        <Card.Body>
                            <Card.Title>Welcome! {this.state.user.Name}</Card.Title>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Name:
                        <input type="text" name="user.Name" value={this.state.user.Name} onChange={this.handleChange} />
                    </label>
                    <label>
                        E-mail:
                        <input type="text" name="user.Email" value={this.state.user.Email} onChange={this.handleChange} />
                    </label>
                    <label style={{ color: 'gray' }}>
                        Credibility:
                        <input readOnly  type="text" name="user.Credibility" value={this.state.user.Credibility} onChange={this.handleChange} />
                    </label>
                    <label>
                        Age:
                        <input type="text" name="user.Age" value={this.state.user.Age} onChange={this.handleChange} />
                    </label>
                    <label>
                        Gender:
                        <select name="user.InterestedGender" value={this.state.user.Gender} onChange={this.handleChange}>
                        {GenderList.map(optn => (
                            <option>{optn}</option>
                        ))}
                        </select>
                    </label>
                    Interested:
                    <div style={{border: "1px solid rgb(202, 202, 202)"}}>
                        
                        <div >Age Min:                              
                        <input type="text" name="user.InterestedAgeMin" value={this.state.user.InterestedAgeMin} onChange={this.handleChange} />
                            Age Max:
                        <input type="text" name="user.InterestedAgeMax" value={this.state.user.InterestedAgeMax} onChange={this.handleChange} />
                        </div>
                    
                         Gender:
                        <select name="user.InterestedGender" value={this.state.user.UnterestedGender} onChange={this.handleChange}>
                        {GenderList.map(optn => (
                            <option>{optn}</option>
                        ))}
                        </select>
                    </div >
                    <div>
                        Attended Events:
                        {
                            this.state.user.Attended.map((bookedEvt) =>
                            <li><Popup trigger={<a href="#popmake-123" class="button">{bookedEvt}</a>} position="top left">
                            {close => (
                            <div>
                                <li>event name:{bookedEvt.Title}</li>
                                <li>event match:{bookedEvt.Recomendations}</li>
                                <a className="close" onClick={close}>
                                &times;
                                </a>
                            </div>
                            )}
                        </Popup></li>
                          )
                        }
                    </div>
                    <div>
                        Booked Events:
                        {
                            this.state.user.Booked.map((bookedEvt) =>
                            <li><Popup trigger={<a href="#popmake-123" class="button">{bookedEvt}</a>} position="top left">
                            {close => (
                            <div>
                                <li>event name:{bookedEvt.Title}</li>
                                
                                <a className="close" onClick={close}>
                                &times;
                                </a>
                            </div>
                            )}
                        </Popup></li>
                          )
                        }
                    </div>
                    <div>
                        Booked Matches:
                        {this.state.user.Booked.map(optn => (
                               <li> {optn} -> {this.state.user.Recommendations[optn]}</li>
                        ))}
                        
                    </div>
                    
                    <br/>
                    {/*<input type="submit" value="Get Update"/>*/}
                </form>
        
          
                            
                        </Card.Body>
                    </Card>
                    
                    </Col>
                    
                    <Col md={2}></Col>
                </Row>
        </Container>
        }
        
        


    </div>);
  }
}

export default MyPage;