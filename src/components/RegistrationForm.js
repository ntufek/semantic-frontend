import React, { Component } from 'react';
import Modal from './modal';
import './RegistrationForm.css';
import {    Link  } from "react-router-dom";
import {Dropdown,DropdownButton, MenuItem} from "react-bootstrap";
import {Container, Row, Col, Card, Button, InputGroup, FormGroup,FormControlLabel, FormControl,Image, Form } from "react-bootstrap";
import User from './user';
import Login from './login/Login'

const GenderList = ["Male", "Female", "Else"];

const activityTypeList=['Dance', 'SportsFilm', 'MuseumTour', 'Party', 'Badminton', 'Theatre', 'BallroomDance', 'TechnoMusic', 'Lunch', 'Hockey', 'VerismoOpera', 'CountryMusic', 'Candidate', 'InterestedActivityType', 'Dinner', 'MusicFilm', 'ThrillerShortFilm', 'SportsFilm', 'WarFilm', 'Name', 'ThrillerFilm', 'PerformingArts', 'JazzDance', 'RomanceTheatre', 'GrandOpera', 'Hockey', 'Athletics', 'Event1', 'Activity', 'ContemporaryBallet', 'RockMusic', 'Credibility', 'RomanticBallet', 'NeoclassicalBallet', 'RomanceTheatre', 'PopMusic', 'MotorSport', 'Conference', 'TableTennis', 'DocumentaryFilm', 'RomanceFilm', 'Basketball', 'FunkMusic', 'DocumentaryFilm', 'Swimming', 'TragicomedyTheatre', 'SeriousOpera', 'ClassicalMusic', 'ComedyTheatre', 'Workshop', 'DramaTheatre', 'BiographyFilm', 'RomanticBallet', 'HiphopMusic', 'ModernDance', 'WarFilm', 'Golf', 'FolkMusic', 'ReggaeMusic', 'BiographyFilm', 'ComedyTheatre', 'Tennis', 'JazzDance', 'RomanceShortFilm', 'ActionFilm', 'CorniqueOpera', 'HiphopDance', 'CorniqueOpera', 'AdventureFilm', 'HeavyMetalMusic', 'AlternativeRockMusic', 'BallroomDance', 'ScienceFictionFilm', 'Workshop', 'Age', 'Attended', 'ComedyShortFilm', 'NoirFilm', 'BluesMusic', 'ElectronicDanceMusic', 'Person1', 'DramaFilm', 'ComicOpera', 'TragedyTheatre', 'BachataDance', 'Breakfast', 'EducationalMeeting', 'AdventureTheatre', 'FunkMusic', 'GrandOpera', 'NoirFilm', 'SemiseriaOpera', 'IndividualSport', 'ActivityType', 'ArtFilm', 'Party', 'Music', 'Event2', 'AdventureTheatre', 'FantasyFilm', 'SoulMusic', 'TangoDance', 'RockMusic', 'RomanticComedyFilm', 'PopMusic', 'Other', 'Event1', 'SeriousOpera', 'DiscoMusic', 'TableTennis', 'WesternFilm', 'CountryMusic', 'Boxing', 'AnimationFilm', 'ThrillerFilm', 'Attendee', 'LatinDance', 'Football', 'Event2', 'ShortFilm', 'ClassicalBallet', 'Lunch', 'PunkRockMusic', 'Boxing', 'Badminton', 'HistoryFilm', 'HeavyMetalMusic', 'Dinner', 'DramaShortFilm', 'HistoryFilm', 'RomanticComedyFilm', 'FantasyFilm', 'Athletics', 'RomanceFilm', 'ClassicalBallet', 'MysteryFilm', 'ComedyFilm', 'CrimeFilm', 'SoulMusic', 'VerismoOpera', 'Basketball', 'ScienceFictionShortFilm', 'Person', 'HorrorFilm', 'ModernDance', 'ThrillerShortFilm', 'ContemporaryBallet', 'ComedyFilm', 'JazzMusic', 'RussianBallet', 'AnimationFilm', 'Meeting', 'Skiing', 'Seminar', 'Ballet', 'SemiseriaOpera', 'Volleyball', 'Gender', 'Exhibition', 'Opera', 'FeatureFilm', 'AnimationShortFilm', 'DramaTheatre', 'ComedyShortFilm', 'ScienceFictionShortFilm', 'Exhibition', 'ActionShortFilm', 'LatinDance', 'TragicomedyTheatre', 'NeoclassicalBallet', 'MysteryFilm', 'InstrumentalMusic', 'WesternFilm', 'IceHockey', 'AnimationShortFilm', 'HorrorFilm', 'TechnoMusic', 'Swimming', 'Person1', 'Golf', 'Location', 'PunkRockMusic', 'Football', 'Booked', 'FamilyFilm', 'CulturalMeeting', 'MusicFilm', 'Event', 'StartDateTime', 'HiphopMusic', 'HouseMusic', 'ClassicalMusic', 'ActionShortFilm', 'HorrorShortFilm', 'TeamSport', 'Volleyball', 'InterestedAgeMax', 'MuseumTour', 'FolkMusic', 'ReggaeMusic', 'FamilyShortFilm', 'Conference', 'MusicalFilm', 'ActionFilm', 'FamilyFilm', 'DiscoMusic', 'DocumentaryShortFilm', 'MusicalFilm', 'ComicOpera', 'BachataDance', 'AdventureFilm', 'ScienceFictionFilm', 'TragedyTheatre', 'IceHockey', 'Other', 'Skiing', 'Sport', 'JazzMusic', 'MotorSport', 'Performer', 'Film', 'RomanceShortFilm', 'Title', 'Seminar', 'ElectronicDanceMusic', 'InterestedGender', 'TangoDance', 'Tennis', 'FamilyShortFilm', 'BluesMusic', 'CustomMeeting', 'DocumentaryShortFilm', 'DramaFilm', 'HiphopDance', 'RussianBallet', 'InterestedAgeMin', 'HorrorShortFilm', 'ArtFilm', 'HouseMusic', 'CrimeFilm', 'Breakfast', 'DramaShortFilm', 'InstrumentalMusic', 'AlternativeRockMusic']

const Checkbox = props => (
  <input type="checkbox" {...props} />
)

class RegistrationForm extends Component {
  constructor(props)
    {
        super(props);
        this.state={
            user: this.GetMyInfo()
        }
        
    }

    GetMyInfo = () =>
    {
        var user = ""
        
        return user;

    }
    SetMyInfo = () =>
    {

        
    }

    handleChange = (e) =>
    {
        console.log("this.state.title",this.state.title,this.state.location)
        console.log("e.name, e.value",e.target.name,e.target.value)
        this.setState({[e.target.name]: e.target.value});
    }
    handleSubmit = (e) =>
    {
        console.log("submitted")
        this.props.history.push('/login');
    }
    
    handleSelectedEvents = (params) =>
    {
      console.log(params);
    }


  render() {
    
    return (
      <div className="container">
        <Container>
                <Row className="show-grid">
                    <Col md={2}></Col>
                    <Col md={8}>
                    <Card>
                       <br/>
                       <Card.Body>
                            <Card.Title>Create Your Free Account</Card.Title>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Name:
                        <input type="text" name="user.name" value={this.state.user.name} onChange={this.handleChange} />
                    </label>
                    <label>
                        E-mail:
                        <input type="text" name="user.email" value={this.state.user.email} onChange={this.handleChange} />
                    </label>
                    <label>
                        Password:
                        <input type="password" name="user.password" value={this.state.user.password} onChange={this.handleChange} />
                    </label>
                    <label>
                        Age:
                        <input type="text" name="user.age" value={this.state.user.age} onChange={this.handleChange} />
                    </label>
                    <label>
                        Gender:
                        <select name="user.interestedGender" value={this.state.user.gender} onChange={this.handleChange}>
                        {GenderList.map(optn => (
                            <option>{optn}</option>
                        ))}
                        </select>
                    </label>
                    Interested:
                    <div style={{border: "1px solid rgb(202, 202, 202)"}}>
                        
                        <div >Age Min:                              
                        <input type="text" name="user.interestedAgeMin" value={this.state.user.interestedAgeMin} onChange={this.handleChange} />
                            Age Max:
                        <input type="text" name="user.interestedAgeMax" value={this.state.user.interestedAgeMax} onChange={this.handleChange} />
                        </div>
                    
                         Gender:
                        <select name="user.interestedGender" value={this.state.user.interestedGender} onChange={this.handleChange}>
                        {GenderList.map(optn => (
                            <option>{optn}</option>
                        ))}
                        </select>
                    </div >
                    <div>
                      Interested Activities:
                      <select name="user.activityType" value={this.state.user.activityType} onChange={this.handleChange}>
                        {activityTypeList.map(optn => (
                            <option>{optn}</option>
                        ))}
                        </select>
                      
                    </div>
                    
                    
                    
                    <br/>
                    <input type="submit" value="Submit"/>
                </form>
        
          
                            
                        </Card.Body>
                    </Card>
                    
                    </Col>
                    
                    <Col md={2}></Col>
                </Row>
        </Container>
      </div>
    );
  } 
  
}


export default RegistrationForm;