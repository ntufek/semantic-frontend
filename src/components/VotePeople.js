import React from "react";
import {Container, Row, Col, Card, Button, InputGroup, FormGroup, FormControl,Image, Form , Table } from "react-bootstrap";
import ReactBasicTable from 'react-basic-table';
import Constants from "./Constants"

export default class VotePeople extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            users:this.getUsers(),
            selectedOption:""
        }

    }
    getUsers = () =>
    { 
        //@TODO: get from server
        let dummyUsers =[
            {
                "username":"osman",
                "eventname":"Tarkan Concert",
                "eventdate":"2018",
                "vote":5
            },
            {
                "username":"burak",
                "eventname":"Lunch ",
                "eventdate":"2001",
                "vote":5
            }
        ]
        return dummyUsers
    }
    handleFormSubmit = (formSubmitEvent) =>
        {
            
            console.log('You have selected:', this.state.users);
            //@TODO: send to server
            
        }
    handleOptionChange = (e) =>
     {
         console.log("user voting vote",e.target.username, e.target.name, e.target.value)
            /*this.setState({
              selectedOption: e.target.value
            });*/
           
            this.state.users[e.target.name].vote =  e.target.value
            this.forceUpdate()
            //this.setState({[e.target.name]: e.target.value});
          }

   

    render()
    {
        var users = this.state.users
        console.log("users", users)
        let tmp = (
            <div>
                {Constants.bannerFunc(this.props.location.state.detail)}
            
            <Container>
                
                <Row>
                    <Col md={2}></Col>
                    <Col md={8}>
                    <form>
                        <Row>
                            <Table >
                                <thead>
                                    <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Event Name</th>
                                    <th>Date</th>
                                    <th>Credibility</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.users.map((thisUser, index) => (
                                    <tr>
                                        <td>{index + 1}</td>
                                        <td>{thisUser.username}</td>
                                        <td>{thisUser.eventname}</td>
                                        <td>{thisUser.eventdate}</td>
                                        <td>
                                            <select name={index} username={thisUser.name} eventname={thisUser.eventname} eventdate={thisUser.eventdate} value={this.state.users[index].selectedOption} onChange={this.handleOptionChange}>
                                                {[1,2,3,4,5,6,7,8,9,10].map(optn => (
                                                    <option>{optn}</option>
                                                ))}
                                            </select>
                                        </td>
                                        
                                    </tr>
                                    ))}
                                    
                                </tbody>
                            </Table>
                        </Row>
                        <Row>
                        <Col>
                        <input value="Submit" type="submit" onClick={this.handleFormSubmit}></input>
                        </Col>
                        </Row>
                    </form>
                    </Col>
                    <Col md={2}></Col>
                </Row>
            </Container>
            </div>
        );
        
        return (<div >
                {tmp}               

        </div>);

    }


}