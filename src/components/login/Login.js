import React, { Component } from "react";
import { Row, FormGroup, FormControl, FormLabel , Button, HelpBlock, Form } from 'react-bootstrap';
import './login.sass';
import { isEmail, isEmpty, isLength, isContainWhiteSpace } from '../../shared/validator';
import RegistrationForm from '../RegistrationForm';
import registerServiceWorker from '../registerServiceWorker';
import ReactDOM from 'react-dom';
import {    Link  } from "react-router-dom";
import Home from "../Home";


class Login extends Component {

    constructor(props) {
        super(props)

        this.state = {
            formData: {}, // Contains login form data
            errors: {}, // Contains login field errors
            formSubmitted: false, // Indicates submit status of login form
            loading: false ,// Indicates in progress state of login form
            name:""
            
        }
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        let { formData } = this.state;
        formData[name] = value;

        
    }
    handleInputChangeName = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        let { formData } = this.state;
        formData[name] = value;

        this.setState({
            name: value
        });
    }

    validateLoginForm = (e) => {

        let errors = {};
        const { formData } = this.state;

        if (isEmpty(formData.email)) {
            //errors.email = "Email can't be blank";
        }

        if (isEmpty(formData.password)) {
            errors.password = "Password can't be blank";
        }  else if (isContainWhiteSpace(formData.password)) {
            errors.password = "Password should not contain white spaces";
        } else if (!isLength(formData.password, { gte: 6, lte: 16, trim: true })) {
            errors.password = "Password's length must between 6 to 16";
        }

        if (isEmpty(errors)) {
            return true;
        } else {
            return errors;
        }
    }

    login = (e) => {

        e.preventDefault();

        let errors = this.validateLoginForm();
        //TODO: back-end e yolla
        if(errors === true){
            
            //alert("You are successfully signed in...");
            //window.location.reload()
            console.log("this.state.name", this.state.name)
            this.props.history.push({pathname:'/home',
            state: { detail: this.state.name }    
                        });
            
            
            
            this.setState({
                errors: errors,
                formSubmitted: true
            });
        } else {
            this.setState({
                errors: errors,
                formSubmitted: true
            });
        }
        
    }
    
    render() {

        const { errors, formSubmitted } = this.state;
        console.log("errors, formSubmitted", errors, formSubmitted);
        return (
            <div>
             <div className="Login">
                <Row>
                    <form onSubmit={this.login}>
                        <FormGroup controlId="email" validationState={ formSubmitted ? (errors.email ? 'error' : 'success') : null }>
                            <FormLabel>User Name</FormLabel>
                            <FormControl type="text" name="name" placeholder="Enter your user name" onChange={this.handleInputChangeName} />
                        { errors.email &&
                            <p class="text-danger">{errors.email}</p>
                        }
                        </FormGroup>
                        <FormGroup controlId="password" validationState={ formSubmitted ? (errors.password ? 'error' : 'success') : null }>
                            <FormLabel>Password</FormLabel>
                            <FormControl type="password" name="password" placeholder="Enter your password" onChange={this.handleInputChange} />
                        { errors.password &&
                            <p class="text-danger">{errors.password}</p>
                        }
                        </FormGroup>
                        <Button type="submit" bsStyle="primary">Sign-In</Button>
                    </form>
                </Row>
                <Row>
                <Link to="/registrationform">
                    <Button>
                        New User
                    </Button>
                </Link>

                </Row>
               
                
                
            </div>
           
            </div>
        )
    }
}

export default Login;