import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './index.sass';
import App from './App';
import registerServiceWorker from './components/registerServiceWorker';

import { BrowserRouter } from 'react-router-dom';
import { Router, Route, hashHistory } from 'react-router';

ReactDOM.render((
  <BrowserRouter>
    <App />
  </BrowserRouter>
  ), document.getElementById('root')
);
registerServiceWorker();
